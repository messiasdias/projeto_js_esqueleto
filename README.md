# Projeto JS Esqueleto 
## Projeto JavaScript Esqueleto com Webpack, VueJS, font-awesome... 


> ### package.json

```
{
  "name": "projeto_js_esqueleto",
  "version": "1.0.0",
  "description": "Projeto JavaScript Esqueleto com Webpack, VueJS, font-awesome... ",
  "main": "src/js/main.js",
  "repository": {
    "type": "git",
    "url": "git+https://github.com/messiasdias/projeto_js_esqueleto.git"
  },
  "keywords": [
    "html",
    "css",
    "sass",
    "scss",
    "vuejs",
    "vue",
    "vuex",
    "javascript",
    "js",
    "npm"
    "webpack"
  ],
  "author": "Messias Dias",
  "license": "ISC",
  "bugs": {
    "url": "https://messiasdias.github.io/projeto_js_esqueleto/issues"
  },
  "homepage": "https://messiasdias.github.io/projeto_js_esqueleto",
  "private": true,
  "dependencies": {
    "vue": "2.6.10",
    "vue-axios": "2.1.5",
    "vue-router": "3.1.3",
    "vuex": "3.1.2"
  },
  "devDependencies": {
    "@vue/cli-plugin-babel": "4.0.0",
    "@vue/cli-plugin-eslint": "4.0.0",
    "@vue/cli-service": "4.0.0",
    "babel-eslint": "10.0.3",
    "babel-loader": "8.0.6",
    "cache-loader": "4.1.0",
    "css-loader": "3.4.0",
    "eslint": "5.16.0",
    "eslint-plugin-vue": "5.0.0",
    "node-sass": "4.13.0",
    "sass-loader": "8.0.0",
    "style-loader": "1.1.2",
    "vue-loader": "15.8.3",
    "vue-style-loader": "4.1.2",
    "vue-template-compiler": "2.6.10",
    "webpack": "4.41.5",
    "webpack-cli": "3.3.10",
    "webpack-dev-server": "^3.10.1"
  },
  "scripts": {
    "serve": "vue-cli-service serve --port 8081 --watch --public http://localhost:8081  ./src/js//main.js",
    "build": "webpack -p ",
    "lint": "vue-cli-service lint"
  },
  "eslintConfig": {
    "root": true,
    "env": {
      "node": true
    },
    "extends": [
      "plugin:vue/essential",
      "eslint:recommended"
    ],
    "rules": {
      "vue/no-use-v-if-with-v-for": "off",
      "no-console": "off"
    },
    "parserOptions": {
      "parser": "babel-eslint"
    }
  },
  "postcss": {
    "plugins": {
      "autoprefixer": {}
    }
  },
  "browserslist": [
    "> 1%",
    "last 2 versions"
  ]
}

```


>### Dependências Extras

Adicionar as preferências manualmente ao arquivo package.json.

```
{
    "dependencies": {
        "@fortawesome/fontawesome-svg-core": "1.2.26",
        "@fortawesome/free-brands-svg-icons": "^5.12.0",
        "@fortawesome/free-solid-svg-icons": "5.12.0",
        "@fortawesome/vue-fontawesome": "0.1.9",
        "animate.css": "3.7.2",
        "font-awesome": "4.7.0",
        "jquery": "3.4.1",
        "typed.js": "^2.0.11",
        "vue-axios": "2.1.5",
    },

    "devDependencies": {

    }
}
```

>### webpack.config.js

```
const { VueLoaderPlugin } = require('vue-loader')
var path = require('path')
var webpack = require('webpack')

module.exports = {
  mode: 'production',
  entry: { 
    main: './src/js/main.js',
    //assets: './vue/js/assets.js'
  },
  output: {
    path: path.resolve(__dirname, './'),
    publicPath: '/',
    filename: './public/assets/js/app-[name].js'
  },
  module: {
    rules: [
  
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: './public/assets/img/[name].[ext]'
        }
      },
      { test: /\.(png|woff|woff2|eot|ttf|svg)$/,
         loader: 'url-loader?limit=100000' ,
         options: {
          name: './public/assets/css/fonts/[name].[ext]'
        }
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(scss|sass)$/,
        use: ['style-loader', 'css-loader','sass-loader']
      },
      {
        test: /\.vue$/,
        use: ['vue-loader']
      },
      
    ]
  },
  plugins:[
    new VueLoaderPlugin()
  ],
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    port: 8080,
    host: '0.0.0.0'
  },
  performance: {
    hints: false
 },
  
  devtool: '#eval-source-map'
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
}
```


## Instalando as Dependências
Com o NodeJs/npm instalado, rodar o comando no diretório do projeto.

```
npm install 
```

## Iniciando Desenvolvimento

Com as dependências instaladas, tenha certeza de haver criado o arquivo ``` src/js/main.js ``` que é 
a entrada da aplicação JavaScript pra o webpack.
Se desejar modificar o nome local do script de entrada (entry file): 


```package.json```
```
"main": "src/js/main.js" 

"scripts": {
  "serve": "vue-cli-service serve --port 8081 --watch --public http://localhost:8081  ./src/js/main.js",
}
  
```

```webpack.config.js```
``` 
entry: { 
    main: './src/js/main.js',
}
```



### Trabalhando com Sass
Se desejar trabalhar com estilo exteno usando Sass, crie o arquivo ``` src/sass/main.scss|sass ```, a partir dele você pode importar outros arquivos .sccs|.sass, além de fonts, background, images... 


#### Se está tudo com citado acima, Agora é hora de iniciar o servidor de desenvolvimento [vue-cli-server](https://cli.vuejs.org/guide/cli-service.html) :
```
npm run serve 
```
O servidor deve iniciar como http://localhost:8081 assistindo suas modificações.

#### Para compilar e gerar os arquivos da distribuição, rode o comando abaixo:
```
npm run build
```


###### Continuar a documentação em Breve ....